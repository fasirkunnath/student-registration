import React from "react";
import "antd/dist/antd.css";
import "./App.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import Login from "./Pages/Login/Login";
import StudentRegistration from "./Pages/StudentRegistration/StudentRegistration";
import Dashboard from "./Pages/Dashboard/Dashboard";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function App() {
  return (
    <div className="App">
      {/* <Login /> */}
      {/* <StudentRegistration /> */}
      {/* <Dashboard /> */}
      <Router>
        <Switch>
          <Route path="/" exact>
            <Login />
          </Route>
          <Route path="/studentregistration">
            <StudentRegistration />
          </Route>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
        </Switch>
      </Router>
      {/* <Router>
        <Route path={"/"} component={Login} />
        <Route path={"studentregister"} component={StudentRegistration} />
      </Router> */}
    </div>
  );
}

export default App;
