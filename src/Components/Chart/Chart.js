import React, { Component } from "react";
import Chart from "react-apexcharts";

class DashboardChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        chart: {
          id: "basic-bar",
          width: "100%",
          height: 380,
        },
        xaxis: {
          categories: [1991, 1992, 1993, 1994, 1995],
        },
      },
      series: [
        {
          name: "series-1",
          data: [30, 40, 45, 50, 49],
        },
      ],

      responsive: [
        {
          breakpoint: undefined,
          options: {},
        },
      ],
    };
  }

  render() {
    return (
      <div className="app">
        <div className="row">
          <div className="mixed-chart">
            <Chart
              options={this.state.options}
              series={this.state.series}
              type="bar"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default DashboardChart;
