import React from "react";
import { Menu } from "antd";
import DashboardIcon from "@material-ui/icons/Dashboard";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import AddCircleOutlineIcon from "@material-ui/icons/AddCircleOutline";
import { Link } from "react-router-dom";
const { SubMenu } = Menu;

class Sidemenu extends React.Component {
  state = {
    collapsed: false,
  };

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <div className="side-menu">
        <div>
          {/* <Button
            type="primary"
            onClick={this.toggleCollapsed}
            style={{ marginBottom: 16 }}
          >
            {React.createElement(
              this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined
            )}
          </Button> */}
          <Menu
            defaultSelectedKeys={["1"]}
            defaultOpenKeys={["sub1"]}
            mode="inline"
            theme="dark"
            inlineCollapsed={this.state.collapsed}
          >
            <Menu.Item key="1" icon={<DashboardIcon />}>
              <Link to="/dashboard"> Dashboard</Link>
            </Menu.Item>
            <Menu.Item key="2" icon={<LockOpenIcon />}>
              <Link to="/">Login</Link>
            </Menu.Item>
            <Menu.Item key="3" icon={<AddCircleOutlineIcon />}>
              <Link to="/studentregistration"> Student Registration</Link>
            </Menu.Item>
          </Menu>
        </div>
      </div>
    );
  }
}

export default Sidemenu;
