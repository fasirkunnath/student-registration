import React from "react";
import { Avatar } from "antd";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Badge from "@material-ui/core/Badge";
import DashboardIcon from "@material-ui/icons/Dashboard";
import Mobilemenu from "../../Components/Mobilemenu/Mobilemenu";

const Topbar = () => {
  return (
    <div>
      <div className="container-fluid top-bar">
        <div className="row">
          <div className="col">
            <img
              className="logo"
              src={require("../../Images/logo-icon.png")}
              alt=""
            />
          </div>
          <div className="col text-right">
            <div className="moile-menu ">
              <Mobilemenu />
            </div>
            <ul>
              <li>
                <DashboardIcon />
              </li>
              <li>
                <Badge badgeContent={4} color="error">
                  <NotificationsIcon />
                </Badge>
              </li>
              <li>
                <Avatar size={30} style={{ backgroundColor: "#001529" }}>
                  F
                </Avatar>
                Fasir
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Topbar;
