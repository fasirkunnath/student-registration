import React, { useState, useEffect } from "react";

import "./Login.scss";
import BackgroundSlider from "react-background-slider";
import { Form, Input, Button, message } from "antd";
import image1 from "../../Images/login_bg.jpg";
import image2 from "../../Images/login_bg2.jpg";
import { Link } from "react-router-dom";
const Login = () => {
  return (
    <div className="bg-color">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-5 mx-auto d-flex  login_bg text-center">
            <div className="align-self-center w-100">
              <div className="login ">
                <img
                  src={require("../../Images/logo.png")}
                  className="login_logo"
                  alt=""
                />

                <Form layout="vertical" size="large" name="control-hooks">
                  <Form.Item
                    label="User Name"
                    name="UserName"
                    rules={[
                      {
                        required: true,
                        message: "Enter UserName.",
                      },
                    ]}
                  >
                    <Input placeholder="User Name" name="UserName" />
                  </Form.Item>
                  <Form.Item
                    label="Password"
                    name="Password"
                    rules={[
                      {
                        required: true,
                        message: "Enter Password.",
                      },
                    ]}
                  >
                    <Input
                      type="password"
                      placeholder="Password"
                      name="Password"
                    />
                  </Form.Item>
                  <Form.Item>
                    <Button type="primary" shape="round" htmlType="submit">
                      <Link to="/dashboard">Submit</Link>
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            </div>
          </div>

          <div className="col-md-7  mx-auto d-flex  text-center">
            <BackgroundSlider
              images={[image1, image2]}
              duration={6}
              transition={1}
            />
            <div className="align-self-center w-100">
              <div className="login-right ">
                <h3>Student Registration</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Aliquam ac maximus libero. Pellentesque in maximus erat.
                  Aenean vitae risus non risus tempus vestibulum. Donec interdum
                  fringilla justo,{" "}
                </p>
                <Button type="primary" shape="round" htmlType="submit">
                  <Link to="/studentregistration">Register Now</Link>
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
