import React from "react";
import Sidemenu from "../../Components/Sidemenu/Sidemenu";
import Topbar from "../../Components/Topbar/Topbar";
import Chart from "../../Components/Chart/Chart";
import { Table, Card, Progress } from "antd";
const columns = [
  {
    title: "Month",
    dataIndex: "name",
    key: "name",
    render: (text) => <div>January</div>,
  },
  {
    title: "Science",
    dataIndex: "age",
    key: "age",
    render: (text) => <div>80</div>,
  },
  {
    title: "Engish",
    dataIndex: "address",
    key: "address",
    render: (text) => <div>80</div>,
  },
  {
    title: "Hindi",
    key: "tags",
    dataIndex: "tags",
    render: (text) => <div>70</div>,
  },
  {
    title: "Maths",
    key: "action",
    render: (text) => <div>100</div>,
  },
];

const data = [
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
    tags: ["nice", "developer"],
  },
  {
    key: "2",
    name: "Jim Green",
    age: 42,
    address: "London No. 1 Lake Park",
    tags: ["loser"],
  },
  {
    key: "3",
    name: "Joe Black",
    age: 32,
    address: "Sidney No. 1 Lake Park",
    tags: ["cool", "teacher"],
  },
];

const Dashboard = () => {
  return (
    <div>
      <Topbar />
      <Sidemenu />
      <div className="content">
        <div className="row">
          <div className="col-md-4">
            <Card
              title="Report"
              bordered={false}
              style={{ width: "100%" }}
              className="dashboard-box"
            >
              <Chart />
            </Card>
          </div>
          <div className="col-md-8">
            <Card
              title="Attendance of last three month"
              bordered={false}
              style={{ width: "100%" }}
              className="dashboard-box"
            >
              <div className="row ">
                <div className="col-md-4 text-center">
                  <Progress type="circle" percent={75} strokeColor="#21defa" />
                  <h6>June</h6>
                </div>

                <div className="col-md-4 text-center">
                  <Progress type="circle" percent={85} strokeColor="#21defa" />
                  <h6>July</h6>
                </div>
                <div className="col-md-4 text-center">
                  <Progress type="circle" percent={90} strokeColor="#21defa" />
                  <h6>August</h6>
                </div>
              </div>
            </Card>
          </div>
          <div className="col-md-12">
            <Card
              title="Mark List"
              bordered={false}
              style={{ width: "100%" }}
              className="dashboard-box"
            >
              <div className="table-responsive">
                <Table columns={columns} dataSource={data} />
              </div>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
