import React from "react";
import "./StudentRegistration.scss";
import BackgroundSlider from "react-background-slider";
import { Form, Input, Button, DatePicker, Select, Upload, message } from "antd";
import image1 from "../../Images/login_bg.jpg";
import image2 from "../../Images/login_bg2.jpg";
import { UploadOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
function onChange(date, dateString) {
  console.log(date, dateString);
}
const { Option } = Select;

function handleChange(value) {
  console.log(`selected ${value}`);
}
const props = {
  name: "file",
  action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
  headers: {
    authorization: "authorization-text",
  },
  onChange(info) {
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};
const { TextArea } = Input;
const StudentRegistration = () => {
  return (
    <div className="bg-color">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-8 mx-auto d-flex  login_bg text-center">
            <div className="align-self-center w-100">
              <div className="student-register ">
                <img
                  src={require("../../Images/logo.png")}
                  className="login_logo"
                  alt=""
                />

                <Form layout="vertical" size="large" name="control-hooks">
                  <div className="row text-left">
                    <div className="col-md-4">
                      <Form.Item label="Name">
                        <Input placeholder="User Name" name="UserName" />
                      </Form.Item>
                    </div>

                    <div className="col-md-4">
                      <Form.Item label="Date of birth ">
                        <DatePicker
                          style={{ width: "100%" }}
                          onChange={onChange}
                        />
                      </Form.Item>
                    </div>

                    <div className="col-md-4">
                      <Form.Item label="Age">
                        <Input placeholder="Age" />
                      </Form.Item>
                    </div>

                    <div className="col-md-4">
                      <Form.Item label="Class">
                        <Input placeholder="Class" />
                      </Form.Item>
                    </div>

                    <div className="col-md-4">
                      <Form.Item label="Roll Number">
                        <Input placeholder="Roll Number" />
                      </Form.Item>
                    </div>

                    <div className="col-md-4">
                      <Form.Item label="Parent Name">
                        <Input placeholder="Parent Name" />
                      </Form.Item>
                    </div>

                    <div className="col-md-4">
                      <Form.Item label="Select Country">
                        <Select
                          defaultValue="India"
                          style={{ width: "100%" }}
                          onChange={handleChange}
                        >
                          <Option value="India">India</Option>
                          <Option value="UAE">Uae</Option>
                        </Select>
                      </Form.Item>
                    </div>

                    <div className="col-md-4">
                      <Form.Item label="Select State">
                        <Select
                          defaultValue="Kerala"
                          style={{ width: "100%" }}
                          onChange={handleChange}
                        >
                          <Option value="Kerala">Kerala</Option>
                          <Option value="Tamilnadu">Tamilnadu</Option>
                        </Select>
                      </Form.Item>
                    </div>

                    <div className="col-md-4">
                      <Form.Item label="Select City">
                        <Select
                          defaultValue="Malappuram"
                          style={{ width: "100%" }}
                          onChange={handleChange}
                        >
                          <Option value="Malappuram">Malappuram</Option>
                          <Option value="Calicut">Calicut</Option>
                        </Select>
                      </Form.Item>
                    </div>

                    <div className="col-md-12">
                      <Form.Item label="Address">
                        <TextArea rows={4} />
                      </Form.Item>
                    </div>

                    <div className="col-md-4">
                      <Form.Item label="Certificate">
                        <Upload {...props}>
                          <Button>
                            <UploadOutlined /> Click to Upload
                          </Button>
                        </Upload>
                      </Form.Item>
                    </div>

                    <div className="col-md-4">
                      <Form.Item label="Photo">
                        <Upload {...props}>
                          <Button>
                            <UploadOutlined /> Click to Upload
                          </Button>
                        </Upload>
                      </Form.Item>
                    </div>
                  </div>

                  <Form.Item>
                    <Button type="primary" shape="round" htmlType="submit">
                      <Link to="/dashboard">Submit</Link>
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            </div>
          </div>

          <div className="col-md-4  mx-auto d-flex  ">
            <BackgroundSlider
              images={[image1, image2]}
              duration={6}
              transition={1}
            />
            <div className="align-self-center w-100 ">
              <div className="login-right text-center">
                <h3>Login</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Aliquam ac maximus libero. Pellentesque in maximus erat.
                  Aenean vitae risus non risu
                </p>
                <Button type="primary" shape="round" htmlType="submit">
                  <Link to="/">Login</Link>
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default StudentRegistration;
